#!/usr/bin/env ruby

class Control

  attr_reader :upright_coord, :rovers, :rovers_coord

  def initialize(*args)
    args = *args
    @rovers = []
    @upright_coord = get_coords(args.slice!(0))

    rovers_attributes = args
    raise "Wrong number of attributes for rovers" if rovers_attributes.length % 2 != 0
    rovers_attributes.each_slice(2) do |rover_args|
      @rovers << Rover.new(max_coord: @upright_coord, start_coord_input: rover_args[0], directions_input: rover_args[1])
    end
  end

  def move_rovers
    raise "No rovers given" if @rovers.empty?
    @rovers_coord = @rovers.map do |rover|
      rover.move
      rover.current_coord.values.join(" ")
    end
  end

  private

  def downleft_coord
    @downleft_coord ||= { x: 0, y: 0 }
  end

  def get_coords(input = "")
    raise "Error with your input for up-right coordinates. Please run again...  (example for up-right coordinates:5 5)" unless input =~ /\d\s\d/
    coord = input.split(" ").map(&:to_i)
    raise "Error with your input for up-right coordinates. They should be bigger than 0. Please run again...  (example for up-right coordinates:5 5)" if coord.include?(0)
    Hash[[:x, :y].zip(coord)]
  end

end

class Rover
  DIRECTIONS = %w(N E S W)

  attr_reader :start_coord, :max_coord, :directions, :current_coord

  def initialize(max_coord: {x: 1, y: 1}, start_coord_input: "", directions_input: "")
    @max_coord = max_coord
    @current_coord = @start_coord = get_coords(start_coord_input)

    @directions = get_directions(directions_input)
  end

  def move
    @directions.each do |d|
      case d
      when 'L', 'R'
        change_direction(d)
      else
        step_forward
      end
    end
  end

  private

  def change_direction(d)
    idx = DIRECTIONS.index(@current_coord[:d])
    if d == 'L'
      idx -= 1
      @current_coord[:d] = idx < 0 ? DIRECTIONS[3] : DIRECTIONS[idx]
    else
      idx += 1
      @current_coord[:d] = idx > 3 ? DIRECTIONS[0] : DIRECTIONS[idx]
    end
  end

  def step_forward
    case @current_coord[:d]
    when 'N'
      @current_coord[:y] += 1
    when 'E'
      @current_coord[:x] += 1
    when 'W'
      @current_coord[:x] -= 1
    when 'S'
      @current_coord[:y] -= 1
    else
      # standing still
    end
    raise "Out Of Bounds ERROR, with coordinates #{@current_coord[:x]}, #{@current_coord[:y]}" if @current_coord[:x] < 0 || @current_coord[:y] < 0 || max_coord[:x] < @current_coord[:x] || max_coord[:y] < @current_coord[:y]
  end

  def get_coords(input = "")
    raise "Error with the start coordinates for rover. Please run again...  (example start coordinates:5 5 E)" unless input =~ /\d\s\d\s[NEWS]/
    input = input.split(" ")
    coord = input[0..1].map(&:to_i)
    direction = input[2]
    coord = Hash[[:x, :y, :d].zip(coord << direction)]
    raise "Out of Bounds starting coordinations for rover" if max_coord[:x] < coord[:x] || max_coord[:y] < coord[:y]
    coord
  end

  def get_directions(input = "")
    raise "Error with the directions for rover. Please run again...  (example for directions: LMLMLMLMM)" unless input =~ /[LRM]+/
    input.split("")
  end
end


# Test Input:
# 5 5
# 1st rover
# 1 2 N
# LMLMLMLMM
# 2nd rover
# 3 3 E
# MMRMMRMRRM

# Expected Output:
# 1 3 N
# 5 1 E



require "minitest/spec"
require "minitest/autorun"

describe "Control" do
  describe "should not initialize" do
    it "when there is wrong num of attributes for 2 rovers" do
      err = proc { Control.new("5 5", "1 2 N", "LMLMLMLMM", "3 3 E") }.must_raise RuntimeError
      err.message.must_match "Wrong number of attributes for rovers"
    end

    it "when attributes move rover out of bounds" do
      err = proc { Control.new("0 0", "1 2 N", "MMMMMM") }.must_raise RuntimeError
      err.message.must_match "Error with your input for up-right coordinates. They should be bigger than 0. Please run again..."
    end
  end

  describe "#move_rovers" do
    describe "should give the new coordinates" do
      it "when attributes for 1 rover" do
        @control_rovers = Control.new("5 5", "1 2 N", "LMLMLMLMM")
        @control_rovers.move_rovers.must_equal ["1 3 N"]
      end

      it "when attributes for 2 rovers" do
        @control_rovers = Control.new("5 5", "1 2 N", "LMLMLMLMM", "3 3 E", "MMRMMRMRRM")
        @control_rovers.move_rovers.must_equal ["1 3 N", "5 1 E"]
      end
    end

    describe "should not give the new coordinates" do
      it "when attributes for 0 rovers" do
        @control_rovers = Control.new("5 5")
        err = proc { @control_rovers.move_rovers }.must_raise RuntimeError
        err.message.must_match "No rovers given"
      end

      it "when attributes move rover out of bounds" do
        @control_rovers = Control.new("5 5", "1 2 N", "MMMMMM")
        err = proc { @control_rovers.move_rovers }.must_raise RuntimeError
        err.message.must_match "Out Of Bounds ERROR, with coordinates 1, 6"
      end
    end

  end
end